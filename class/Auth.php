<?php

use Firebase\JWT\JWT;
require_once './class/php-jwt-master/src/JWT.php';
require './class/php-jwt-master/src/SignatureInvalidException.php';
require './class/php-jwt-master/src/ExpiredException.php';
require './class/php-jwt-master/src/BeforeValidException.php';


class Auth {
	public static function signIn($user,$pass){

	try {

		if($user=="Cristhofer" && $pass=="123"){
		
			$time= time(); //tiempo y hora actual
			$key="cuenta_key";
			$token=array(
				'iat' => $time,
				'exp' =>$time + (60*60),
				'usuario' => $user,
			);
			$jwt=JWT::encode($token,$key);
			setcookie("cuenta_cookie", $jwt, time()+3600);
			return true;
			
		}else{

			return false;
		}

		} catch (Exception $e) {
				return false;
		}
	}

	public static  function verif(){
		try {
				$jwt=$_COOKIE['cuenta_cookie'];
				$decoded= JWT::decode($jwt,'cuenta_key',array('HS256'));
				return $decoded;
		} catch (Exception $e) {
			return false;
		}

	}
}