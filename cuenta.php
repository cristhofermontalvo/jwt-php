<?php
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once './class/Auth.php';
$result=Auth::verif();
if($result==false){
	setcookie ("cuenta_cookie", "", time() - 3600);
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<title>Bienvenido</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<hr>
		<h1>Home page</h1>
		<h2>Bienvenido <?php echo $result->usuario?> </h2>
		<hr>
		<a href="logout.php">
			<button style="text-decoration: none;color:black;background: white;border-radius: 5px;padding: 10px;border: 1px solid #999">Cerrar sesion</button>
		</a>
	</div>

</body>
</html>
